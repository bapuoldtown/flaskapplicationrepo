from flask import Flask 
import datetime
import time


app=Flask('__name__')
@app.route('/',methods=['GET','POST'])
def home():
    return '<h1>Hello From Flask and now is {}</h2>'.format(time.strftime("%H:%M:%S",time.localtime()))

@app.route('/<string:name>',methods=['GET','POSt'])
def greet(name):
    return '<h1>Hello {} From Flask and now is {}</h2>'.format(name.title(),time.strftime("%H:%M:%S",time.localtime()))

@app.route('/cicd')
def cicdroue():
    return '<h1>This is an extension of the CI/CD Handson'

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
    


